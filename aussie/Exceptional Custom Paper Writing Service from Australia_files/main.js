$(function(){
    // hacks
    if (window.innerWidth < 480) {
        var mvp = document.getElementById('vp');
        mvp.setAttribute('content', 'width=480, initial-scale=1');
    }

    //menu main
    var main_menu  = $('.main-menu ul');

    $('.main-menu__btn-block').click(function(){

        $(this).toggleClass('js-menu-view');
        main_menu.slideToggle();

    });

    $('#pay_button').click(function () {
        $('.paymenttype_popup').slideToggle('fast');
        return false;
    });


    $('.prices-table td:nth-child(n+2)').bind('mouseover', onPriceMouseover)
        .bind('mouseout', onPriceMouseout);

    function onPriceMouseover() {
        var $td = $(this);
        $td.addClass('active');
    }

    function onPriceMouseout() {
        var $td = $(this);
        $td.removeClass('active');
    }

    /*$('.testimonials-slider ul').bxSlider({
     minSlides: 1,
     maxSlides: 1,
     slideMargin: 20,
     auto: true,
     pause: 10000,
     autoHover: true
     });*/

    $('.show-login-window').bind('click', function (e) {
        e.preventDefault();
        $('.login-window').addClass('show');
        return false;
    });

    $('.login-window .close').bind('click', function (e) {
        e.preventDefault();
        $('.login-window').removeClass('show');
        return false;
    });

    $(document).bind('click touchstart', function (e) {
        if (!$(e.target).closest('.login-window').length) {
            $('.login-window').removeClass('show');
        }
    });

    $('.faq-list li').bind('click', function (e) {
        var th = $(this),
            an = th.find('.answer'),
            $target = $(e.target),
            isTextClicked = $target.hasClass('answer');

        th.siblings('.opened').find('.answer').slideToggle(200).end().removeClass('opened');

        if (isTextClicked) return;

        an.slideToggle(200);
        th.toggleClass('opened');
    });

    /*$('.filter .row select').chosen();*/

    /*$('select.chosen').chosen();*/

    $('.extras-select ul li .name').bind('click', function (e) {
        $(this).closest('li').toggleClass('opened').find('.item-content').slideToggle(300);
    });

    $('.more-wrap .trigger').bind('click', function (e) {
        e.preventDefault();

        var $btn = $(this);

        if ($btn.hasClass('animating'))  return;

        $btn.addClass('animating');

        $('.trigger-list').slideToggle(function () {
            $btn.removeClass('animating');
        });
    });

    $(document.body).bind('click', hideMobileMenu);
    $('.wrap, .page-header, .page-footer, .page-content').bind('click', hideMobileMenu);

    function hideMobileMenu(e) {
        var $target = $(e.target),
            isMoreLink = $target.hasClass('trigger');

        if (isMoreLink) return;

        $('.trigger-list').slideUp();
    }

    $('.js-addwriterid').bind('click', onAddWriterClick);
    $('.form-content_writerid .js-del').bind('click', onDelClick);

    function onAddWriterClick(e) {
        e.preventDefault();

        var $btn = $(this),
            $input = $btn.siblings('.middle:first'),
            $clonedInput = $input.clone().val(''),
            $delBtn = $('<a/>').addClass('add-btn add-btn_del js-del').attr('href', '#'),
            inputsAddedQty = $('.form-content_writerid .js-del').size(),
            $lastInput;

        if (inputsAddedQty === 9) return;

        console.log(inputsAddedQty);

        if (inputsAddedQty) {
            $lastInput = $btn.siblings('.middle:last + .add-btn');
        } else {
            $lastInput = $btn.siblings('.middle:last');
        }

        $clonedInput.insertAfter($lastInput);
        $delBtn.insertAfter($clonedInput);
        $btn.insertAfter($input);
    }

    function onDelClick(e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.prev('.middle').remove();
        $btn.remove();
    }

//$('.select_country + .chosen-container [data-option-array-index]').bind('click', onCountrySelectClick);

//function onCountrySelectClick(e) {
//    var $item = $(this),
//        itemValue = $item.data('optionArrayIndex'),
//        countriesWithState = [1, 3],
//        containsStates = !!(~countriesWithState.indexOf(itemValue));
//
//    if (containsStates) {
//        $('.select_province').attr('disabled', false).trigger("chosen:updated");
//    } else {
//        $('.select_province').attr('disabled', true).trigger("chosen:updated");
//    }
//}

    $('.minus').bind('click', function (e) {
        e.preventDefault();

        var $btn = $(this),
            $input = $btn.closest('.counter').find('input'),
            currentValue = +$input.val();

        if (currentValue === 1) return;

        $input.val(--currentValue);
    });

    $('.plus').bind('click', function (e) {
        e.preventDefault();

        var $btn = $(this),
            $input = $btn.closest('.counter').find('input'),
            currentValue = $input.val();

        $input.val(++currentValue);
    });

    $('.info-popup').bind('click', function () {
        var $container = $(this),
            $popup = $container.find('.ttip'),
            $popups = $('.ttip'),
            pageWidth = $(window).width();

        if (pageWidth > 480) return;

        $popups.removeClass('ttip-visible');
        $popup.toggleClass('ttip-visible');
    });

    $(document.body).bind('click', hidePopup);
    $('.wrap, .page-header, .page-footer, .page-content').bind('click', hidePopup);

    function hidePopup(e) {
        var $target = $(e.target),
            isPopup = $target.closest('.info-popup').size();

        if (isPopup) return;

        $('.ttip').removeClass('ttip-visible');
    }

    $('.currency-list a').bind('click', function (e) {
        e.preventDefault();

        var $btn = $(this);
        $btn.addClass('active');
        $btn.siblings('a').removeClass('active');
    });

    safariLayoutFix();

    function safariLayoutFix() {
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

        if (!isSafari) return;

        $('html').addClass('safari');

        var $orderForm = $('#order_form');
        if (!$orderForm.length)  return;

        setTimeout(function () {
            $('#order_form').find('.form-label')
                .css({float: 'none'});
        }, 1);
    }
});

$(function () {
    $('input[type="text"]').bind('keyup', function (e) {
        if (e.keyCode !== 13) return;

        $(this).blur();
    });
});

$(function () {
    var $popup = $('#fancybox-wrap'),
        $overlay = $('#fancybox-overlay');

    if (!$overlay.length || !$overlay.is(':visible')) return;

    $popup.wrap('<div class="fancybox-outer-wrap"/>');
    $overlay.wrap('<div class="fancybox-outer-wrap"/>');

    $('#fancybox-close').bind('click', function () {
        $('.fancybox-outer-wrap').remove();
    });
});

$(function () {
    var isDeadlinePage = $('.payment-resubmit-wrap').length;
    if (!isDeadlinePage) return;

    $('.page-content').addClass('page-content_deadline');
});

$(function() {
    var $previewExtras = $('.preview .order-form-item-content_extras');
    if (!$previewExtras.length) return;

    $previewExtras.find('.sweet-checkbox a').attr({'href': 'javascript:void(0)'});
});

$(function() {
    var isDeadlinePage = $('.page-content_deadline').length;
    if (!isDeadlinePage) return;
    $('html, body').css({height: '100%'});
});

$(function() {
    var $popup = ('.popUpWrap'),
        isFeedbackPage = $popup.length;

    if (!isFeedbackPage) return;

    $('.page-content').addClass('page-content_feedback');
    $('.popUpWrap').addClass('popUpWrap_feedback');
});

$(function() {
    var isWebkit = 'WebkitAppearance' in document.documentElement.style,
        $sitemap = $('.sitemap');
    if (!isWebkit || !$sitemap.length)  return;
    $('html').addClass('webkit');
});

$(function() {
    // remove stars on resubmit page

    var isResubmitPage = $('#details').is(':hidden') && $('.page-content_feedback').length;

    if (!isResubmitPage) return;

    $('.page-content').addClass('resubmit_unpaid');

});
