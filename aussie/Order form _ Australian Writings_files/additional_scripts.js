/**
 * Created by Ekaterina.Porkhun on 20.01.2015.
 */
var trigger = false;
function setTrigger() {
    if(trigger== false) {
        $('.popup').removeClass('popup');
    }
    else {
        trigger = false;
    }
}



// функции для имитации placeholder
var focusField = function(el, value){
    if((typeof value != 'string') && (el.val()=='')) {
        value.hide();
    }
    else if ((typeof value == 'string') && (el.val()==value)) {
        el.val('');
    }

}
var blurField = function(el, value){
    if((typeof value != 'string') && (el.val()=='')) {
        value.show();
    }
    else if ((typeof value == 'string') && (el.val()=='')) {
        el.val(value);
    }
}

/**
 * функция для переключателей формы
 * @param e
 */
var formSwitcher=function(e){
    var $element = $(e);
    this.init = function(){
        $element.bind('click', function(){
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
        });
    }
};


var orderFormPh = function(o){
    var inputElement = document.createElement('input');
    if(!('placeholder' in inputElement)) {
        for (var i = 0; i< o.length; i++) {
            (function(i){
                $(o[i].elem).val(o[i].value)
                    .bind('focus', function(){
                        focusField($(this), o[i].value)
                    }).bind('blur', function(){
                        blurField($(this), o[i].value)
                    });
            })(i);
        };

    }
};

$(function(){
    $('.forgot-pass-wrap').bind('click', function(){
       trigger = true;
    });
    $('.login-link').bind('click', function(){
        $('.loginRight').addClass('popup');
        trigger = true;
    });
    $('.loginRight').bind('click', function(e){
       trigger = true;

        if($(e.target).hasClass('forgot-link')) {
            $(e.target).parent().parent().parent().removeClass('popup');
            $('.forgot-pass-wrap').addClass('popup');
            e.preventDefault();
        }
    });

    $('.content .close').bind('click', function(){
        $(this).parent().parent().removeClass('popup');
    })
    var togglePlaceholder = function(){
        if($(window).width()<= 758) {
            $('.loginRight').find('.text-field').removeAttr('placeholder');
        }
        else {
            $('.loginRight').find('.text-field').attr('placeholder', 'Login')
                .end().find('input[type="password"]').attr('placeholder', '********');
        }
    }
    togglePlaceholder();
    $(window).bind('resize', function(){
        togglePlaceholder();
    });
    var $menu = $('.mobile-menu');
    $('.more-toggle').bind('click', function(){
        if(!$menu.hasClass('visible')) {
            $menu.addClass('visible')
                .find('a').each(function(i){
                    var self = $(this);
                    setTimeout(function(){self.addClass('visible');
                        if(i == $menu.find('a').length-1) {
                            $('.more-toggle').addClass('menu-show');
                        }
                    }, 100+i*200);

                });
        }
        else {
            $($menu.find('a').get().reverse()).each(function(i){
                var self = $(this);
                setTimeout(function(){self.removeClass('visible');
                    if(i == $menu.find('a').length-1) {
                        $menu.removeClass('visible');
                        $('.more-toggle').removeClass('menu-show');
                    }
                }, 150+i*200);

            });
        }
        return false;
    });
    if($(window).width()<= 758) {
        $('.headr_bg, .content').bind('click', function(){
            setTrigger();
        });
    }
    else {
        $('body').bind('click', function(){
            setTrigger();
        } );

    }
    //select cstomize
    /*$('.t-select').customize();*/

    // td selection in price table
    $('.eotc_price-table a')
        .bind('mouseenter', function(){
            var tableIndex = 0;
            switch ($(this).parent().parent().index()) {
                case 1:
                    tableIndex = 1;
                    break;
                case 2:
                    tableIndex = 2;
                    break;
                case 3 :
                    tableIndex = 3;
                    break;
            }
            $(this).parentsUntil('tbody').find('.u-cell').addClass('selected');

        })
        .bind('mouseleave',  function(){
            $(this).parentsUntil('tbody').find('.u-cell').removeClass('selected');
        });

    var switcher = new formSwitcher('.form-switcher');
    switcher.init();

    orderFormPh(
        [{elem:'#firstname', value: 'First name'}, {elem:'#lastname', value: 'Last name'},
            {elem:'#phone1_area', value: 'area'}, {elem:'#phone1_number', value: 'number'}]

    );

});