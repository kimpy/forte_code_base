/**
 * Created by Ekaterina.Porkhun on 04.11.2014.
 */
var selectTransform = function(a){
    var $select =$(a), selectClasses = $select.attr('class');
    $select.hide();
    var $customSelect = $('<div>').addClass('select-wrap').addClass(selectClasses).removeClass('t-select').insertAfter($select);
    var $selectValue = $('<div>').addClass('select-value')
        .append(
        $('<div>').addClass('select-text').html((($select.find('option').length)? $select.find('option:selected').html() : '&nbsp;'))
    )
        .append(
        $('<div>').addClass('select-value-arr')
    )
        .appendTo($customSelect);



    //creating option list
    if($select.find('option').length) { // if select is not empty do:

        //creating options wrap
        var $selectOptions = $('<div>').addClass('select-options').appendTo($customSelect);
        var optionsWrap = '.select-options';


        /**
         * function for opening pseudo-select
         * @param a
         */
        var openSelect = function(a){
            var optList = $(a).parent().find(optionsWrap);
            if(optList.hasClass('popup')) {
                $(optionsWrap).removeClass('popup');
                trigger = false;
            }
            else {
                $(optionsWrap).removeClass('popup');
                trigger = true;
                optList.addClass('popup');
            }
        };

        /**
         * select checked option
         */
        var changeSelect = function(a, b){
            var $el = $(a), $opt = $(b);
            $el.parent().parent().find('.select-text').html($el.html()).end().find(optionsWrap).removeClass('popup');
            $opt.attr('selected', 'selected');
            trigger=false;
            $select.trigger('change');
        }


        $selectValue.bind('click', function(){ // bind onSelect method
            openSelect(this);
        });

        $select.find('option').each(function(){ // put options values into pseudo-select
            var self = this;
            var $el = $('<div>').addClass('select-option')
                .html($(self).text())
                .bind('click', function(){
                    $(this).parent().find('.selected').removeClass('selected').end().end().addClass('selected');
                    changeSelect(this, self);
                })
                .appendTo($selectOptions);
            if($(self).attr('selected')) {
                $el.addClass('selected');
            }
        });
    }
    else {
        $customSelect.addClass('disabled');
    }
};
$.fn.extend({
    customize : function(){
        return this.each(function(){
            selectTransform(this);
        });
    }
});
